# MIMEMail

## What's this?

mimemail is a small utility for creating raw emails with attachments.

Example: `mimemail --sender "User <user@example.com>" --recipient "Somebody <somebody@somewhere.com>" --recipient "friend@invalid.xyz" --subject "Greetings from MIMEMail" --message "Check out these cool attachments!" --content-language "fi-FI" --attachment /path/to/some/file --attachment /path/to/some/other/file`

## USAGE

mimemail [options]

To create a sane email, you need to specify the following parameters:

* --sender  (one)
* --recipient (one or more)
* --subject (one)
* --message (one)
* --content-language (one, in format ex `en-US`)
* --attachment (one or more)

In case you want to define more than one of the aformentioned supported parameters, simply repeat it, for instance `--recipient somebody@somewhere.xyz --recipient somebody@else.invalid`

To send the mail, be sure to have an MTA locally and run for instance `mimemail $PARAMETERS | sendmail -t`

## Author

Axel Latvala
